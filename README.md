## YelpApp

---

YelpApp is an application that uses the Yelp Fusion API and it is built using UIKit. The goal is to create a master-detail application using MVVM architecture.
The application displays a list of business listings obtained from the Yelp API and show a detailed view for each item.

The app uses core location as coordinates are required for the Yelp fusion API.

## List and Detail views

---

The list view will display a ThumbImage, Business Name, Category, Address and Rating, while the detail view shows a large banner image, displaying name, category, address, contact number, ratings, and operating hours.

## Search Capabilities

---

Once the user taps the search field, he can search by business name, by addres/city/postal, by cuisine type, it can also sort by rating and distance

## Clone, Build and Run

---

To clone the repo use the git clone command with the following link

```
git clone https://ryanmigallos@bitbucket.org/ryanmigallos/yelpapp.git
```

Then change your directory to the project root folder

```
$ cd yelpapp/YelpApp
```

## Development tools and technology stack

---

The YelpApp was developed using the following tools and Techstack:

* Xcode 11.5 (latest version)
* Swift 5
* MVVM (ListingsViewController)
* Autolayout
* Combine Framework (Reactive Paradigm)
* Codable Framework
* Protocol Oriented Programming (POP) and Object Oriented Programming (OOP)

## Platforms

---

Currently YelpApp only supports iPhone platforms.