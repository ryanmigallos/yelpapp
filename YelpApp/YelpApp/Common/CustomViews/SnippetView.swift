//
//  SnippetView.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

class SnippetView: UIView, ViewConfigurable {
    
    private let rating: UILabel = {
        let component = SnippetView.label(color: .white, size: 20, weight: .medium)
        component.text = "0.0"
        component.textAlignment = .center
        component.backgroundColor = UIColor(red: 0.00, green: 0.72, blue: 0.58, alpha: 1.00)
        return component
    }()
    
    private let reviewCount: UILabel = {
        let component = SnippetView.label(color: .darkText, size: 14, weight: .regular)
        component.text = "0"
        component.textAlignment = .center
        component.backgroundColor = .white
        return component
    }()
    
    private let reviewText: UILabel = {
        let component = SnippetView.label(color: .darkText, size: 8, weight: .light)
        component.text = "REVIEWS"
        component.textAlignment = .center
        component.backgroundColor = .white
        return component
    }()
    
    private let vStack: UIStackView = {
        let component = UIStackView()
        component.axis = .vertical
        component.alignment = .fill
        component.distribution = .fillProportionally
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        
        layer.cornerRadius = 4
        layer.borderWidth = 0.5
        layer.borderColor = UIColor(red: 0.87, green: 0.90, blue: 0.91, alpha: 1.00).cgColor
        clipsToBounds = true
        
        vStack.addArrangedSubview(rating)
        vStack.addArrangedSubview(reviewCount)
        vStack.addArrangedSubview(reviewText)
        addSubview(vStack)
        configureComponents()
    }
    
    private func configureComponents() {
        NSLayoutConstraint.activate([
            vStack.topAnchor.constraint(equalTo: topAnchor),
            vStack.leadingAnchor.constraint(equalTo: leadingAnchor),
            vStack.trailingAnchor.constraint(equalTo: trailingAnchor),
            vStack.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    public func configureWith(item: Business) {
        guard let ratingValue = item.rating,
            let reviewValue = item.reviewCount else { return }
        rating.text = String(format: "\(ratingValue)")
        reviewCount.text = String(format: "\(reviewValue)")
    }
}
