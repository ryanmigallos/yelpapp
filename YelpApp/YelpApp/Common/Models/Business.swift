//
//  Business.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/2/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import Foundation

// MARK: - Response
public struct Response: Codable {
    let businesses: [Business]?
    let total: Int?
    let region: Region?
}

// MARK: - Business
public struct Business: Codable {
    let id: String?
    let alias: String?
    let name: String?
    let imageURL: String?
    let isClaimed: Bool?//
    let isClosed: Bool?
    let url: String?
    let reviewCount: Int?
    let categories: [Category]?
    let rating: Double?
    let coordinates: Coordinates?
    let transactions: [String]?
    let price: String?
    let location: Location?
    let phone: String?
    let displayPhone: String?
    let distance: Double?
    let hours: [Hour]?//
    let photos: [String]?//
    
    enum CodingKeys: String, CodingKey {
        case id
        case alias
        case name
        case imageURL = "image_url"
        case isClaimed = "is_claimed"//
        case isClosed = "is_closed"
        case url
        case reviewCount = "review_count"
        case categories
        case rating
        case coordinates
        case transactions
        case price
        case location
        case phone
        case displayPhone = "display_phone"
        case distance
        case hours//
        case photos//
    }
}

extension Business: Hashable {
    
    public static func == (lhs: Business, rhs: Business) -> Bool {
        guard let leftId = lhs.id,
            let rightId = rhs.id
            else { return false }
        
        return leftId == rightId
    }
    
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
    
    var displayLocation: String {
        guard let location = location else { return "" }
        return location.textAddress
    }
    
    var displayCategories: String {
        guard let categories = categories else { return ""}
        let text = categories.map { $0.title ?? "" }.joined(separator: " - ")
        return text
    }
    
    var operatingHours: String {
        guard let hours = hours else { return "operation"}
        return hours.map { $0.opening }.joined(separator: "")
    }
}

// MARK: - Category
public struct Category: Codable {
    let alias: String?
    let title: String?
}

// MARK: - Center
public struct Center: Codable {
    let latitude: Double?
    let longitude: Double?
}

// MARK: - Coordinates
struct Coordinates: Codable {
    let latitude: Double?
    let longitude: Double?
}

// MARK: - Location
public struct Location: Codable {
    let address1: String?
    let address2: String?
    let address3: String?
    let city: String?
    let zipCode: String?
    let country: String?
    let state: String?
    let displayAddress: [String]?
    
    enum CodingKeys: String, CodingKey {
        case address1
        case address2
        case address3
        case city
        case zipCode = "zip_code"
        case country
        case state
        case displayAddress = "display_address"
    }
}

extension Location {
    var textAddress: String {
        guard let text = displayAddress else { return "" }
        return text.joined(separator: " ")
    }
}

// MARK: - Region
public struct Region: Codable {
    let center: Center?
}

// MARK: - Hour
struct Hour: Codable {
    let hourOpen: [Open]?
    let hoursType: String?
    let isOpenNow: Bool?

    enum CodingKeys: String, CodingKey {
        case hourOpen = "open"
        case hoursType = "hours_type"
        case isOpenNow = "is_open_now"
    }
    
    var opening: String {
        guard let hourOpen = hourOpen else { return "" }
        var string = ""
        
        hourOpen.forEach { open in
            string += open.openingDescription
            string += "\n"
        }
        
        return string
    }
}

// MARK: - Open
struct Open: Codable {
    let isOvernight: Bool?
    let start: String?
    let end: String?
    let day: Int?

    enum CodingKeys: String, CodingKey {
        case isOvernight = "is_overnight"
        case start
        case end
        case day
    }
}

extension Open: DateProvider {
    
    var openingDescription: String {
        
        let startValue = start ?? "0"
        let endValue = end ?? "0"
        let dayValue = day ?? 0
        
        let formatter = DateFormatter()
        
        let startTime = Open.inHours(value: startValue, formatter: formatter)
        let endTime = Open.inHours(value: endValue, formatter: formatter)
        let dayOfTheWeek = Open.inDayOfWeek(value: dayValue, formatter: formatter)

        return String(format: "\(dayOfTheWeek) \(startTime) - \(endTime)")
    }
}

// MARK: - Section Type
public enum SectionType: CaseIterable {
    case section
}
