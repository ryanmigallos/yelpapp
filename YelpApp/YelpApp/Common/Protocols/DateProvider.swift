//
//  DateProvider.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import Foundation

public protocol DateProvider { }

extension DateProvider {
    
    public static func inHours(value: String, formatter: DateFormatter) -> String {
        let calendar = Calendar.current
        let components = DateComponents(calendar: calendar, hour: Int(value))
        let date = calendar.date(from: components)!
        formatter.dateFormat = "h a"
        return formatter.string(from:date)
    }
    
    public static func inDayOfWeek(value: Int, formatter: DateFormatter) -> String {
        let calendar = Calendar.current
        let components = DateComponents(calendar: calendar, day: value)
        let date = calendar.date(from: components)!
        formatter.dateFormat = "EEEE"
        return formatter.string(from:date)
    }
}
