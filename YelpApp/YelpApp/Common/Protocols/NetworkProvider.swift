//
//  NetworkProvider.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/2/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import Foundation
import Combine
import CoreLocation

public protocol NetworkProvider { }

extension NetworkProvider {
    
    private static func createRequest(from url: URL?) -> URLRequest {
        var request = URLRequest(url: url!)
        let authorization = String(format: "Bearer \(apikey)")
        request.addValue(authorization, forHTTPHeaderField: "Authorization")
        return request
    }
    
    public static func fetchList(request: URLRequest) -> AnyPublisher<[Business]?, Never> {
        let publisher = URLSession.shared.dataTaskPublisher(for: request)
            .map(\.data)
            .decode(type: Response.self, decoder: JSONDecoder())
            .map(\.businesses)
            .replaceError(with: [Business]())
            .eraseToAnyPublisher()
        return publisher
    }
    
    public static func requestSearch(_ keyword: String? = nil,
                                     location: String? = nil,
                                     coordinate: CLLocationCoordinate2D? = nil,
                                     sort: SortType = .rating,
                                     openNow: Bool = false) -> URLRequest {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.yelp.com"
        urlComponents.path = "/v3/businesses/search"
        
        var queryItems = [URLQueryItem]()
        
        // Term
        if let term = keyword {
            queryItems.append(URLQueryItem(name: "term", value: term))
        }
        
        // Use Device Location
        var latitude = "37.786882"
        var longitude = "-122.399972"
        if let userLocation = coordinate {
            print("switching to user location...")
            latitude = "\(userLocation.latitude)"
            longitude = "\(userLocation.longitude)"
        }
        queryItems.append(URLQueryItem(name: "latitude", value: latitude))
        queryItems.append(URLQueryItem(name: "longitude", value: longitude))
        
        // Sort By Rating
        queryItems.append(URLQueryItem(name: "sort_by", value: sort.rawValue))
        
        urlComponents.queryItems = queryItems
        return createRequest(from: urlComponents.url)
    }
}

extension NetworkProvider {
    
    public static func fetchBusiness(id: String) -> Future<Business?, Never> {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.yelp.com"
        urlComponents.path = "/v3/businesses/\(id)"
        let request = createRequest(from: urlComponents.url)
        return Future { promise in
            let task = URLSession.shared.dataTask(with: request) { (data, _, _) in
                let decoder = JSONDecoder()
                if let data = data, let business = try? decoder.decode(Business.self, from: data) {
                    promise( .success(business))
                }
            }
            task.resume()
        }
    }
}

public enum SortType: String {
    case rating
    case distance
}
