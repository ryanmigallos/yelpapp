//
//  ImageDownloadable.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/3/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

public protocol ImageDownloadable {
    func fetchImage(with path: String) -> Future<UIImage?, Never>
}

extension ImageDownloadable {
    public func fetchImage(with path: String) -> Future<UIImage?, Never> {
        return Future { promise in
            if let url = URL(string: path) {
                URLSession.shared.dataTask(with: url) { (data, response, error) in
                    // Error
                    if let error = error {
                        print("error: \(error.localizedDescription)")
                    }
                    // Data
                    if let data = data {
                        let image = UIImage(data: data)
                        promise( .success(image))
                    }
                }.resume()
            }
        }
    }
}
