//
//  ViewConfigurable.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit

public protocol ViewConfigurable { }

extension ViewConfigurable {
    
    public static func label(color: UIColor, size: CGFloat, weight: UIFont.Weight) -> UILabel {
        let component = UILabel()
        component.numberOfLines = 0
        component.textColor = color
        component.textAlignment = .left
        component.font = .systemFont(ofSize: size, weight: weight)
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }
}
