//
//  LocationServices.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import Foundation
import Combine
import CoreLocation

public enum LocationStatus {
    case authorize(CLLocationCoordinate2D)
    case permission(String)
}

class LocationServices: NSObject, CLLocationManagerDelegate {
    
    public let coordinateObservable = PassthroughSubject<LocationStatus, Never>()
    
    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.showsBackgroundLocationIndicator = true
        manager.allowsBackgroundLocationUpdates = true
        return manager
    }()
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    public func retriveCurrentLocation() {
        
        let status = CLLocationManager.authorizationStatus()
        
        if(status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled()){
            // show alert to user telling them they need to allow location data to use some feature of your app
            return
        }
        
        // if haven't show location permission dialog before, show it to user
        if(status == .notDetermined){
            locationManager.requestAlwaysAuthorization()
            return
        }
        
        // at this point the authorization status is authorized
        // request location data once
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            coordinateObservable.send(.authorize(location.coordinate))
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("fail: \(error.localizedDescription)")
        coordinateObservable.send(.permission("failed"))
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            manager.requestLocation()
        case .notDetermined:
            manager.requestAlwaysAuthorization()
        case .denied, .restricted:
            manager.requestAlwaysAuthorization()
        default:
            print("error")
        }
    }
}
