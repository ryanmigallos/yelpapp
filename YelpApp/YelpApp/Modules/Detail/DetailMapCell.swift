//
//  DetailMapCell.swift
//  YelpApp
//
//  Created by Ryan Migallos Bell on 5/1/21.
//  Copyright © 2021 Ryan Migallos. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DetailMapCell: UITableViewCell {
    
    private enum Constants {
        static let distance: CLLocationDistance = 650
        static let pitch: CGFloat = 30
        static let heading = 90.0
    }
    
    public static let identifier = "map-identifier"
    
    // Camera
    private var camera: MKMapCamera!
    
    private let map: MKMapView = {
        let component = MKMapView()
        component.mapType = .hybridFlyover
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        contentView.addSubview(map)
        configureComponents()
    }
    
    private func configureComponents() {
        let margin = contentView.layoutMarginsGuide
        NSLayoutConstraint.activate([
            map.topAnchor.constraint(equalTo: margin.topAnchor),
            map.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            map.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            map.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        ])
    }
    
    public func updateLocation(using business: Business?) {
        guard let business = business,
              let businessName = business.name,
              let coordinates = business.coordinates,
              let latitude = coordinates.latitude,
              let longitude = coordinates.longitude
        else { return }
        
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(
            latitude: latitude,
            longitude: longitude
        )

        // Camera
        self.camera = MKMapCamera(
            lookingAtCenter: centerCoordinate,
            fromDistance: Constants.distance,
            pitch: Constants.pitch,
            heading: Constants.heading)
        map.camera = camera
        
        // Region
        let coordinateRegion = MKCoordinateRegion(center: centerCoordinate, latitudinalMeters: 200, longitudinalMeters: 200)
        let region = map.regionThatFits(coordinateRegion)
        map.setRegion(region, animated: true)
        
        // Coordinate
        annotation.coordinate = centerCoordinate
        annotation.title = businessName
        map.addAnnotation(annotation)
    }
    
    public func animateFlyOver() {
        UIView.animate(withDuration: 20.0) { [weak self] in
            guard let self = self else { return }
            self.camera.heading += 180
            self.camera.pitch = 25
            self.map.camera = self.camera
        }
    }
}
