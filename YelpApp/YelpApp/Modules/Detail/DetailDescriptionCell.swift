//
//  DetailDescriptionCell.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

class DetailTextViewCell: UITableViewCell, ViewConfigurable {
    
    public static let identifier = "text-identifier"
    
    private let header: UILabel = {
        return DetailTextViewCell.label(color: .black, size: 24, weight: .bold)
    }()

    private let category: UILabel = {
        return DetailTextViewCell.label(color: .darkGray, size: 12, weight: .semibold)
    }()
    
    private let address: UILabel = {
        return DetailTextViewCell.label(color: .gray, size: 12, weight: .light)
    }()
    
    private let contact: UILabel = {
        return DetailTextViewCell.label(color: .gray, size: 12, weight: .light)
    }()
    
    private let snippet: SnippetView = {
        let component = SnippetView()
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    private let vStack: UIStackView = {
        let component = UIStackView()
        component.spacing = 8.0
        component.axis = .vertical
        component.alignment = .fill
        component.distribution = .equalSpacing
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
        
    private var cancellables = Set<AnyCancellable>()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        
        vStack.addArrangedSubview(header)
        vStack.addArrangedSubview(category)
        vStack.addArrangedSubview(address)
        vStack.addArrangedSubview(contact)
        
        contentView.addSubview(vStack)
        contentView.addSubview(snippet)
        
        NSLayoutConstraint.activate([
            vStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            vStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            vStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50),
            vStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            snippet.widthAnchor.constraint(equalToConstant: 50),
            snippet.heightAnchor.constraint(equalToConstant: 80),
            snippet.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            snippet.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8)
        ])
    }
    
    public func configureWith(item: Business?) {
        guard let item = item else { return }
        
        header.text = item.name
        category.text = item.displayCategories
        address.text = item.displayLocation
        contact.text = item.displayPhone
        
        snippet.configureWith(item: item)
    }
}
