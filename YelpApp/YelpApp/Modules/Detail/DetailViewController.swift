//
//  DetailViewController.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/3/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

public enum ComponentType {
    case banner
    case header
    case hours
    case map
    
    var height: CGFloat {
        switch self {
        case .banner: return 250.0
        case .header: return UITableView.automaticDimension
        case .hours: return 100.0
        case .map: return 400.0
        }
    }
}

class DetailViewController: UIViewController {

    public var business: Business? = nil
    
    private var components: [ComponentType] = [.banner, .header, .hours, .map]
    
    private let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        table.allowsSelection = false
        table.allowsMultipleSelection = false
        table.register(DetailImageCell.self, forCellReuseIdentifier: DetailImageCell.identifier)
        table.register(DetailTextViewCell.self, forCellReuseIdentifier: DetailTextViewCell.identifier)
        table.register(DetailHourCell.self, forCellReuseIdentifier: DetailHourCell.identifier)
        table.register(DetailMapCell.self, forCellReuseIdentifier: DetailMapCell.identifier)
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detail"
        initialize()
    }
    
    private func initialize() {
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        configureComponents()
    }
    
    private func configureComponents() {
        let margin = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: margin.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        ])
        
        let insets = UIEdgeInsets(top: 0, left: view.frame.width, bottom: 0, right: 0)
        tableView.separatorInset = insets
    }
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return components.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let type = components[indexPath.row]
        
        let defaultCell = UITableViewCell()
        
        switch type {
        case .banner:
            let identifier = DetailImageCell.identifier
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? DetailImageCell else { return defaultCell }
            
            if let business = business, let path = business.imageURL {
                cell.configureImage(path: path)
            }
            
            return cell
            
        case .header:
            let identifier = DetailTextViewCell.identifier
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? DetailTextViewCell else { return defaultCell }
            
            cell.configureWith(item: business)
            
            return cell
            
        case .hours:
            let identifier = DetailHourCell.identifier
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? DetailHourCell else { return defaultCell }
            
            if let businessID = business?.id {
                cell.configureWith(id: businessID)
            }
            
            return cell
        
        case .map:
            let identifier = DetailMapCell.identifier
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? DetailMapCell else { return defaultCell }
            
            if let business = business {
                cell.updateLocation(using: business)
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let type = components[indexPath.row]
        return type.height
    }
}
