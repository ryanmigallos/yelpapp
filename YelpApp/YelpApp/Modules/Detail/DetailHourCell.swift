//
//  DetailHourCell.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

class DetailHourCell: UITableViewCell, ViewConfigurable, NetworkProvider {
    
    public static let identifier = "hour-identifier"
    
    private let hours: UITextView = {
        let component = UITextView()
        component.isEditable = false
        component.isSelectable = false
        component.isScrollEnabled = false
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    private var cancellables = Set<AnyCancellable>()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        contentView.addSubview(hours)
        configureComponents()
    }
    
    private func configureComponents() {
        let margin = contentView.layoutMarginsGuide
        NSLayoutConstraint.activate([
            hours.topAnchor.constraint(equalTo: contentView.topAnchor),
            hours.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            hours.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            hours.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        ])
    }
    
    public func configureWith(id: String) {
        DetailHourCell.fetchBusiness(id: id)
            .receive(on: RunLoop.main)
            .map { business -> String? in
                business?.operatingHours
            }
            .assign(to: \.text, on: hours)
            .store(in: &cancellables)
    }
}
