//
//  DetailImageCell.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

class DetailImageCell: UITableViewCell, ImageDownloadable {
    
    public static let identifier = "image-cell"
    
    private let imageBanner: UIImageView = {
        let component = UIImageView()
        component.contentMode = .scaleAspectFill
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    private var cancellables = Set<AnyCancellable>()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        contentView.clipsToBounds = true
        contentView.addSubview(imageBanner)
        configureComponents()
    }
    
    private func configureComponents() {
        NSLayoutConstraint.activate([
            imageBanner.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageBanner.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageBanner.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageBanner.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    public func configureImage(path: String) {
        fetchImage(with: path)
            .receive(on: RunLoop.main)
            .sink { [weak imageBanner] image in
                guard let component = imageBanner else { return }
                component.image = image
        }
        .store(in: &cancellables)
    }
}
