//
//  ListingViewController.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/3/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

class ListingViewController: UIViewController, UITableViewDelegate {
    
    public typealias DataSource = UITableViewDiffableDataSource<SectionType, Business>
    
    // Diffable DataSource
    private var dataSource: DataSource?
    
    // Search View Controller
    private let searchController: UISearchController = {
        let component = UISearchController(searchResultsController: nil)
        component.obscuresBackgroundDuringPresentation = false
        component.searchBar.placeholder = "Business name, Address or Cuisine"
        component.searchBar.scopeButtonTitles = [SortType.rating.rawValue, SortType.distance.rawValue]
        return component
    }()
    
    // Table View
    private let tableView: UITableView = {
        let component = UITableView(frame: .zero, style: .plain)
        component.rowHeight = 80
        component.register(ListingItemViewCell.self, forCellReuseIdentifier: ListingItemViewCell.identifier)
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    var listings: [Business]? {
        didSet { updateDataSource(with: listings) }
    }
    
    private var viewModel = ListingViewModel()
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Listings"
        configureComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
        setupViewModel()
        setupTableView()
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            guard let controller = segue.destination as? DetailViewController,
                let indexPath = tableView.indexPathForSelectedRow,
                let dataSource = dataSource,
                let business = dataSource.itemIdentifier(for: indexPath) else { return }
            controller.business = business
            // Remove Selection
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: nil)
    }
}

// MARK: - Setup
extension ListingViewController {
    
    private func setupViewModel() {
        // Load Initial List
        viewModel.loadList()
        
        // Bind SearchField
        viewModel.bind(searchField)
        
        // Observe Listings
        viewModel.listingsObservable
            .receive(on: RunLoop.main)
            .assign(to: \.listings, on: self)
            .store(in: &cancellables)
    }
    
    private func setupTableView() {
        dataSource = createDataSource()
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    private func configureComponents() {
        view.addSubview(tableView)
        let margin = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: margin.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        ])
        
        let insets = UIEdgeInsets(top: 0, left: view.frame.width, bottom: 0, right: 0)
        tableView.separatorInset = insets
    }
    
    public var searchField: UISearchTextField {
        return searchController.searchBar.searchTextField
    }
    
    private func setupNavigationBar() {
        definesPresentationContext = true
        navigationController?.navigationBar.prefersLargeTitles = true
        searchController.searchResultsUpdater = self
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}

extension ListingViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let selectedIndex = searchBar.selectedScopeButtonIndex
        guard let scopeTitles = searchBar.scopeButtonTitles else { return }
        let category = scopeTitles[selectedIndex]
        if let type = SortType(rawValue: category) {
            viewModel.loadList(sortType: type)
        }
    }
}

// MARK: - Data Source
extension ListingViewController {
    
    func createDataSource() -> UITableViewDiffableDataSource<SectionType, Business> {
        let identifier = ListingItemViewCell.identifier
        typealias Provider = ((UITableView, IndexPath, Business) -> ListingItemViewCell?)
        let cellProvider: Provider = { tableView, indexPath, business in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ListingItemViewCell
                else { return ListingItemViewCell() }
            cell.configureWith(item: business)
            return cell
        }
        return UITableViewDiffableDataSource(tableView: tableView, cellProvider: cellProvider)
    }
    
    // Update SnapShot
    private func updateDataSource(with items: [Business]?) {
        guard let dataSource = dataSource, let items = items else { return }
        var snapshot = NSDiffableDataSourceSnapshot<SectionType, Business>()
        snapshot.deleteAllItems()
        snapshot.appendSections(SectionType.allCases)
        snapshot.appendItems(items, toSection: .section)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}
