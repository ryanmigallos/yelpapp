//
//  ListingViewModel.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/3/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine
import CoreLocation

class ListingViewModel: NetworkProvider {

    // Term
    var term = "deli"
    
    // Location Services
    private let locationService: LocationServices
    private var deviceLocation: CLLocationCoordinate2D? {
        didSet { loadList(coordinate: deviceLocation) }
    }
    
    // Subscription Cancellables
    private var cancellables = Set<AnyCancellable>()
    public var listingsObservable: PassthroughSubject<[Business]?, Never>
    
    // Text Change Notification
    let textChangeNotif = UISearchTextField.textDidChangeNotification
    
    init() {
        locationService = LocationServices()
        listingsObservable = PassthroughSubject<[Business]?, Never>()
        
        locationService.retriveCurrentLocation()
        
        locationService.coordinateObservable
            .receive(on: RunLoop.main)
            .map { status -> CLLocationCoordinate2D? in
                switch status {
                case .authorize(let location):
                    return location
                case .permission(let message):
                    print("message error: \(message)")
                    return nil
                }
            }
            .assign(to: \.deviceLocation, on: self)
            .store(in: &cancellables)
    }
    
    public func bind(_ searchField: UISearchTextField) {
        NotificationCenter.default
            .publisher(for: textChangeNotif, object: searchField)
            .map { (notif: Notification) -> String in
                guard let searchField = notif.object as? UISearchTextField,
                    let text = searchField.text else { return "" }
                return text
        }
        .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
        .removeDuplicates()
        .map { [weak self] term in
            self?.term = term
            return ListingViewModel
                .requestSearch(term, coordinate: self?.deviceLocation)
        }
        .flatMap { return ListingViewModel.fetchList(request: $0) }
        .sink { [weak listingsObservable] (items: [Business]?) in
            guard let subject = listingsObservable else { return }
            subject.send(items)
        }
        .store(in: &cancellables)
    }
    
    public func loadList(coordinate: CLLocationCoordinate2D? = nil) {
        let request = ListingViewModel.requestSearch(term, coordinate: coordinate, sort: .rating, openNow: true)
        print("request: \(request)")
        ListingViewModel
            .fetchList(request: request)
            .sink { [weak listingsObservable] (items: [Business]?) in
                guard let subject = listingsObservable else { return }
                subject.send(items)
        }
        .store(in: &cancellables)
    }
    
    public func loadList(sortType: SortType) {
        let request = ListingViewModel.requestSearch(term, coordinate: deviceLocation, sort: sortType, openNow: true)
        print("request: \(request)")
        ListingViewModel
            .fetchList(request: request)
            .sink { [weak listingsObservable] (items: [Business]?) in
                guard let subject = listingsObservable else { return }
                subject.send(items)
        }
        .store(in: &cancellables)
        
    }
    
}
