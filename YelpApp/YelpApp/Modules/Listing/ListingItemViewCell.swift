//
//  ListingItemViewCell.swift
//  TestSearch
//
//  Created by Ryan Migallos on 6/4/20.
//  Copyright © 2020 Ryan Migallos. All rights reserved.
//

import UIKit
import Combine

class ListingItemViewCell: UITableViewCell, ViewConfigurable {
    
    public static let identifier = "cell-identifier"
    
    private let imageComponent: UIImageView = {
        let component = UIImageView()
        component.clipsToBounds = true
        component.layer.cornerRadius = 4.0
        component.layer.borderWidth = 0.5
        component.layer.borderColor = UIColor(red: 0.87, green: 0.90, blue: 0.91, alpha: 1.00).cgColor
        component.contentMode = .scaleAspectFill
        component.translatesAutoresizingMaskIntoConstraints = false
        component.backgroundColor = UIColor(red: 1.00, green: 0.76, blue: 0.03, alpha: 1.00)
        return component
    }()
    
    private let header: UILabel = {
        return ListingItemViewCell.label(color: .black, size: 14, weight: .bold)
    }()

    private let category: UILabel = {
        return ListingItemViewCell.label(color: .gray, size: 12, weight: .regular)
    }()

    private let address: UILabel = {
        return ListingItemViewCell.label(color: .gray, size: 12, weight: .regular)
    }()
    
    private let rating: UILabel = {
        let component = ListingItemViewCell.label(color: .white, size: 12, weight: .bold)
        component.clipsToBounds = true
        component.layer.cornerRadius = 4
        component.textAlignment = .center
        component.backgroundColor = UIColor(red: 0.00, green: 0.72, blue: 0.58, alpha: 1.00)
        return component
    }()
    
    private let vStack: UIStackView = {
        let component = UIStackView()
        component.axis = .vertical
        component.alignment = .fill
        component.distribution = .fillEqually
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    private let hStack: UIStackView = {
        let component = UIStackView()
        component.spacing = 10
        component.axis = .horizontal
        component.alignment = .center
        component.distribution = .fill
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
        
    private var cancellables = Set<AnyCancellable>()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        imageComponent.image = nil
    }
}

// MARK: - Setup
extension ListingItemViewCell {
    
    private func initialize() {
        
        vStack.addArrangedSubview(header)
        vStack.addArrangedSubview(category)
        vStack.addArrangedSubview(address)

        hStack.addArrangedSubview(imageComponent)
        hStack.addArrangedSubview(vStack)
        
        contentView.addSubview(hStack)
        contentView.addSubview(rating)
        
        NSLayoutConstraint.activate([
            imageComponent.widthAnchor.constraint(equalToConstant: 70),
            hStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            hStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            hStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50),
            hStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            
            rating.widthAnchor.constraint(equalToConstant: 30),
            rating.heightAnchor.constraint(equalToConstant: 24),

            rating.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            rating.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16)
        ])
    }
}

// MARK: - Image Fetching
extension ListingItemViewCell: ImageDownloadable {
    
    public func configureWith(item: Business) {
        guard let path = item.imageURL else { return }
        
        header.text = item.name
        category.text = item.displayCategories
        address.text = item.displayLocation
        rating.text = String(format: "\(item.rating ?? 0.0)")
        
        fetchImage(with: path)
            .receive(on: RunLoop.main)
            .sink { [weak imageComponent] image in
                guard let component = imageComponent else { return }
                component.image = image
        }
        .store(in: &cancellables)
    }
}
